from django import forms


class CardForm(forms.Model):
    series = models.CharField(max_length=30)
    number = models.CharField(max_length=30)
    issue_date = models.DateTimeField(default=timezone.now)
    expire_date = models.DateTimeField(default=timezone.now)
    used_date = models.DateTimeField(default=timezone.now)
    amount = models.FloatField()
    status = models.ForeignKey(Status, on_delete=models.CASCADE, default=1)

    def __unicode__(self):
        return self.number
