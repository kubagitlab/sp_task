import json
from datetime import datetime

from django.http import HttpResponse
from django.shortcuts import render
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt

from card.models import *


def index(request):
    cards = Card.objects.all()
    statuses = Status.objects.all()
    return render(request, 'card/index.html', {'cards': cards, 'statuses': statuses})


@csrf_exempt
def add(request):
    data = request.POST
    card = Card()
    card.series = data['series']
    card.number = data['number']
    card.issue_date = datetime.strptime(data['issue_date'], '%d.%m.%Y')
    card.expire_date = datetime.strptime(data['expire_date'], '%d.%m.%Y')
    card.status_id = data['status']
    card.amount = 1
    card.save()

    return get_response()


@csrf_exempt
def search(request):
    return get_response(request)


@csrf_exempt
def delete(request):
    data = request.POST
    Card.objects.get(pk=data['id']).delete()
    return get_response()


def get_response(request=None):
    data = request.POST if request else None
    cards = Card.objects.all()
    if data and data.get('status', ''):
        cards = cards.filter(status=data['status'])
    html = render_to_string('card/list.html', {'cards': cards})
    return HttpResponse(json.dumps({'html': html}), content_type="application/json")
