from django.conf.urls import patterns, url
# from django.urls import path

from card import views

urlpatterns = patterns('',
                       url(r'^$', views.index, name='index'),
                       url(r'^add/$', views.add),
                       url(r'^search/$', views.search),
                       url(r'^delete/$', views.delete),
                       )
